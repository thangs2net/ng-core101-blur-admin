/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.monitoring')
      .directive('leagues', leagues);

  /** @ngInject */
  function leagues() {
    return {
      restrict: 'E',
      controller: 'leaguesCtrl',
      templateUrl: 'app/pages/monitoring/leagues/leagues.html'
    };
  }
})();