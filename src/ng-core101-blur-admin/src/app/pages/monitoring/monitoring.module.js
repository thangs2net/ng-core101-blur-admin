/**
 * @author thangs2.net
 * created on 23.09.2016
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.monitoring', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('monitoring', {
          url: '/monitoring',
          templateUrl: 'app/pages/monitoring/monitoring.html',
          title: 'Live Monitoring',
          sidebarMeta: {
            icon: 'ion-android-home',
            order: 0,
          },
        });
  }

})();
