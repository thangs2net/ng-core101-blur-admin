/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.monitoring')
      .directive('eventOddsHistories', eventOddsHistories);

  /** @ngInject */
  function eventOddsHistories() {
    return {
      restrict: 'E',
      controller: 'eventOddsHistoriesCtrl',
      templateUrl: 'app/pages/monitoring/eventOdds/histories/eventOddsHistories.html'
    };
  }
})();