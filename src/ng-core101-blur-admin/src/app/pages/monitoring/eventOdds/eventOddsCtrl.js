/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('BlurAdmin.pages.dashboard')
        .controller('eventOddsCtrl', eventOddsCtrl);

    /** @ngInject */
    function eventOddsCtrl($scope, $uibModal, leaguesService) {

        $scope.eventOdds = [{
            type: 'text-message',
            author: 'Kostya',
            surname: 'Danovsky',
            header: 'Posted new message',
            text: 'Guys, check this out: \nA police officer found a perfect hiding place for watching for speeding motorists. One day, the officer was amazed when everyone was under the speed limit, so he investigated and found the problem. A 10 years old boy was standing on the side of the road with a huge hand painted sign which said "Radar Trap Ahead." A little more investigative work led the officer to the boy\'s accomplice: another boy about 100 yards beyond the radar trap with a sign reading "TIPS" and a bucket at his feet full of change.',
            time: 'Today 11:55 pm',
            ago: '25 minutes ago',
            expanded: false,
        }, {
            type: 'video-message',
            author: 'Andrey',
            surname: 'Hrabouski',
            header: 'Added new video',
            text: '"Vader and Me"',
            preview: 'app/feed/vader-and-me-preview.png',
            link: 'https://www.youtube.com/watch?v=IfcpzBbbamk',
            time: 'Today 9:30 pm',
            ago: '3 hrs ago',
            expanded: false,
        }, {
            type: 'image-message',
            author: 'Vlad',
            surname: 'Lugovsky',
            header: 'Added new image',
            text: '"My little kitten"',
            preview: 'app/feed/my-little-kitten.png',
            link: 'http://api.ning.com/files/DtcI2O2Ry7A7VhVxeiWfGU9WkHcMy4WSTWZ79oxJq*h0iXvVGndfD7CIYy-Ax-UAFCBCdqXI4GCBw3FOLKTTjQc*2cmpdOXJ/1082127884.jpeg',
            time: 'Today 2:20 pm',
            ago: '10 hrs ago',
            expanded: false,
        }, {
            type: 'image-message',
            author: 'Vlad',
            surname: 'Lugovsky',
            header: 'Added new image',
            text: '"My little kitten"',
            preview: 'app/feed/my-little-kitten.png',
            link: 'http://api.ning.com/files/DtcI2O2Ry7A7VhVxeiWfGU9WkHcMy4WSTWZ79oxJq*h0iXvVGndfD7CIYy-Ax-UAFCBCdqXI4GCBw3FOLKTTjQc*2cmpdOXJ/1082127884.jpeg',
            time: 'Today 2:20 pm',
            ago: '10 hrs ago',
            expanded: false,
        }, {
            type: 'image-message',
            author: 'Vlad',
            surname: 'Lugovsky',
            header: 'Added new image',
            text: '"My little kitten"',
            preview: 'app/feed/my-little-kitten.png',
            link: 'http://api.ning.com/files/DtcI2O2Ry7A7VhVxeiWfGU9WkHcMy4WSTWZ79oxJq*h0iXvVGndfD7CIYy-Ax-UAFCBCdqXI4GCBw3FOLKTTjQc*2cmpdOXJ/1082127884.jpeg',
            time: 'Today 2:20 pm',
            ago: '10 hrs ago',
            expanded: false,
        }, {
            type: 'image-message',
            author: 'Vlad',
            surname: 'Lugovsky',
            header: 'Added new image',
            text: '"My little kitten"',
            preview: 'app/feed/my-little-kitten.png',
            link: 'http://api.ning.com/files/DtcI2O2Ry7A7VhVxeiWfGU9WkHcMy4WSTWZ79oxJq*h0iXvVGndfD7CIYy-Ax-UAFCBCdqXI4GCBw3FOLKTTjQc*2cmpdOXJ/1082127884.jpeg',
            time: 'Today 2:20 pm',
            ago: '10 hrs ago',
            expanded: false,
        }, {
            type: 'image-message',
            author: 'Vlad',
            surname: 'Lugovsky',
            header: 'Added new image',
            text: '"My little kitten"',
            preview: 'app/feed/my-little-kitten.png',
            link: 'http://api.ning.com/files/DtcI2O2Ry7A7VhVxeiWfGU9WkHcMy4WSTWZ79oxJq*h0iXvVGndfD7CIYy-Ax-UAFCBCdqXI4GCBw3FOLKTTjQc*2cmpdOXJ/1082127884.jpeg',
            time: 'Today 2:20 pm',
            ago: '10 hrs ago',
            expanded: false,
        }, {
            type: 'image-message',
            author: 'Vlad',
            surname: 'Lugovsky',
            header: 'Added new image',
            text: '"My little kitten"',
            preview: 'app/feed/my-little-kitten.png',
            link: 'http://api.ning.com/files/DtcI2O2Ry7A7VhVxeiWfGU9WkHcMy4WSTWZ79oxJq*h0iXvVGndfD7CIYy-Ax-UAFCBCdqXI4GCBw3FOLKTTjQc*2cmpdOXJ/1082127884.jpeg',
            time: 'Today 2:20 pm',
            ago: '10 hrs ago',
            expanded: false,
        }];

        var vm = this;

        vm.standardSelectItems = [
            { label: 'Option 1', value: 1 },
            { label: 'Option 2', value: 2 },
            { label: 'Option 3', value: 3 },
            { label: 'Option 4', value: 4 },
        ];

        vm.selectWithSearchItems = [
            { label: 'Hot Dog, Fries and a Soda', value: 1 },
            { label: 'Burger, Shake and a Smile', value: 2 },
            { label: 'Sugar, Spice and all things nice', value: 3 },
            { label: 'Baby Back Ribs', value: 4 },
        ];

        vm.groupedSelectItems = [
            { label: 'Group 1 - Option 1', value: 1, group: 'Group 1' },
            { label: 'Group 2 - Option 2', value: 2, group: 'Group 2' },
            { label: 'Group 1 - Option 3', value: 3, group: 'Group 1' },
            { label: 'Group 2 - Option 4', value: 4, group: 'Group 2' },
        ];


        // paginations
        $scope.DEFAULT_PAGE_NO = 1;
        $scope.currentPage = $scope.DEFAULT_PAGE_NO;
        $scope.sortTable = function(name) {
            if (name === $scope.sortBy) {
                $scope.reverse = !$scope.reverse;
            } else {
                $scope.reverse = false;
            }
            $scope.sortBy = name;
            $scope.refresh();
        };
        // paginations
        $scope.refresh = function() {
            $scope.currentPage = $scope.DEFAULT_PAGE_NO;
        };
        // loading
        // $scope.$watch('loading', function(flag) {
        //     if (flag) $loading.start('loading');
        //     else $loading.finish('loading');
        // });

        // search event odds
        $scope.searchStringFilter = function(item) {
            var reg = new RegExp($scope.searchString, 'i');

            if ($scope.searchString && $scope.searchString !== '') {
                return item.type.match(reg) || item.author.match(reg) || item.surname.match(reg);
            }

            return true;
        };
        // filters
        $scope.marketFilter = function(item) {
            if (!$scope.marketCriteria || $scope.marketCriteria === '') {
                return true;
            }
            return !!item[$scope.marketCriteria];
        };

        $scope.getEventOdds = function() {

        };

        
        // modals
        $scope.open = function (msg, page, size) {
            $uibModal.open({
                animation: true,
                // templateUrl: page,
                templateUrl: 'app/pages/shared/modalTemplates/alertModalTemplate.html',
                // templateUrl: 'partials/create.html',
                controller: ['$scope', function ($scope) {
                   console.log('param msg: ' + msg);
                   $scope.msg = msg;
                }]
            });
        };

        $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 5;
        $scope.init = function() {
            $scope.reverse = false;
        };
        $scope.init();

    }
})();