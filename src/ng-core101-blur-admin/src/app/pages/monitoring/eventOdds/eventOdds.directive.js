/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.monitoring')
      .directive('eventOdds', eventOdds);

  /** @ngInject */
  function eventOdds() {
    return {
      restrict: 'E',
      controller: 'eventOddsCtrl',
      templateUrl: 'app/pages/monitoring/eventOdds/eventOdds.html'
    };
  }
})();