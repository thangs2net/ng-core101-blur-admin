(function(){
    'use strict';
    
    angular.module('BlurAdmin.pages')
        .factory('leaguesService', function($q, $http) {

            var canceler = $q.defer();
            var leaugeApi = {};

            var items = [];

            
            leaugeApi.addItem = function(item) {
                items.push(item);
            };
            leaugeApi.removeItem = function(item) {
                var index = items.indexOf(item);
                items.splice(index, 1);
            };
            leaugeApi.updateItem = function(item){
                items.forEach(function(d){
                    if(d.id === item.id){
                        d.isSelected = item.isSelected;
                    }
                });
            };
            leaugeApi.getItems = function() {
                console.log('getItems from service');
                return items;
            };

            leaugeApi.clear = function() {
                items = [];
            };

            leaugeApi.initialData = function(data) {
                items = data;
            };

            leaugeApi.getLeagues = function (params) {
                return $http.post('/api/pos/rooms', params, { timeout: canceler.promise });
            };

            leaugeApi.abort = function () {
                canceler.resolve();
                canceler = $q.defer();
            };
            
            return leaugeApi;
        });
})();