(function(){
    
    'use strict';

    angular.module('BlurAdmin.pages').directive('selectAllCheckbox', function () {
        return {
            replace: true,
            restrict: 'E',
            scope: {
                checkboxes: '=',
                allselected: '=allSelected',
                allclear: '=allClear'
            },
            template: '<label class="checkbox-inline custom-checkbox nowrap"> ' +
                            '<input type="checkbox" ng-model="cbamaster" ng-change="cbaMasterChange()"> '+
                            '<span>Select All</span>' +
                        '</label>',
            controller: function ($rootScope, $scope, $element) {
/*
<input type="checkbox" ng-model="cbamaster" ng-change="cbaMasterChange()">
*/
/*
<label class="checkbox-inline custom-checkbox nowrap">
            <input ng-model="options.closeButton" type="checkbox" id="closeButton">
            <span>Close Button</span>
          </label>
*/
/*
<label class="checkbox-inline custom-checkbox nowrap">
                            <input type="checkbox" ng-model="cbamaster" ng-change="cbaMasterChange()">
                            <span>{{text}}</span>
                        </label>
 */
                $scope.cbaMasterChange = function () {
                    
                    if ($scope.cbamaster) {
                        angular.forEach($scope.checkboxes, function (cb) {
                            cb.isSelected = true;
                        });
                    } else {
                        angular.forEach($scope.checkboxes, function (cb) {
                            cb.isSelected = false;
                        });
                    }
                };

                $scope.$watch('checkboxes', function () {
                    var allSet = true,
                        allClear = true;
                    angular.forEach($scope.checkboxes, function (cb) {
                        if (cb.isSelected) {
                            allClear = false;
                        } else {
                            allSet = false;
                        }
                    });
                    $rootScope.$broadcast('selectedAllLeagues', allSet);

                    if ($scope.allselected !== undefined) {
                        $scope.allselected = allSet;
                    }
                    if ($scope.allclear !== undefined) {
                        $scope.allclear = allClear;
                    }

                    $element.prop('indeterminate', false);
                    if (allSet) {
                        $scope.cbamaster = true;
                    } else if (allClear) {
                        $scope.cbamaster = false;
                    } else {
                        $scope.cbamaster = false;
                        $element.prop('indeterminate', true);
                    }
                    
                }, true);
            }
        };
    });
})();