(function(){
    'use strict';
    
    angular.module('BlurAdmin.pages')
        .factory('eventOddsService', function($q, $http) {

            var canceler = $q.defer();
            var api = {};

            var items = [];

            
            api.addItem = function(item) {
                items.push(item);
            };
            api.removeItem = function(item) {
                var index = items.indexOf(item);
                items.splice(index, 1);
            };
            api.updateItem = function(item){
                items.forEach(function(d){
                    if(d.id === item.id){
                        d.isSelected = item.isSelected;
                    }
                });
            };
            api.getItems = function() {
                
                return items;
            };

            api.clear = function() {
                items = [];
            };

            api.initialData = function(data) {
                items = data;
            };

            api.getEventOdds = function (params) {
                return $http.post('/api/pos/rooms', params, { timeout: canceler.promise });
            };

            api.abort = function () {
                canceler.resolve();
                canceler = $q.defer();
            };
            
            return api;
        });
})();