/**
 * @author thangs2.net
 * created on 23.09.2016
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.favourite', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('favourite', {
          url: '/favourite',
          templateUrl: 'app/pages/favourite/favourite.html',
          title: 'My Favourite',
          sidebarMeta: {
            icon: 'ion-compose',
            order: 0,
          },
        });
  }

})();
